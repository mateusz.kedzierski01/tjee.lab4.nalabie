package lab4.war;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.rmi.PortableRemoteObject;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import lab4.ebj.DataAccessBeanRemote;

/**
 *
 * @author Mefju
 */
public class TestClient extends HttpServlet {

    //@EJB
    //private DataAccessBeanRemote bean;

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     * @throws javax.naming.NamingException
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException, NamingException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet TestClient</title>");            
            out.println("</head>");
            out.println("<body>");
            
            //Stanowy komponent EJB
            //###
            HttpSession session = request.getSession();
            DataAccessBeanRemote bean = (DataAccessBeanRemote) session.getAttribute("ejb");
            if (bean == null) {
             InitialContext ctx = new InitialContext();
             Object obj = ctx.lookup("lab4.ebj.DataAccessBeanRemote");
             bean = (DataAccessBeanRemote) PortableRemoteObject.narrow(obj, DataAccessBeanRemote.class);
             session.setAttribute("ejb", bean);
            }
            //###
            //Bezstanowy komponent EJB
            //###
            //InitialContext ctx = new InitialContext();
            //Object obj = ctx.lookup("lab4.ebj.DataAccessBeanRemote");
            //DataAccessBeanRemote bean = (DataAccessBeanRemote) PortableRemoteObject.narrow(obj, DataAccessBeanRemote.class);
            //###

            
            
            out.println("<b>Ostatni dostęp: " + bean.getOstatnieDane()+ "</b><br/>");
            bean.setOstatnieDane(request.getHeader("user-agent"), new java.util.Date());
            
            bean.increaseLicznik();
            out.println("<b>Stronę odwiedzono " + bean.getLicznik()+ " razy.</b><br/>");
            
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (NamingException ex) {
            Logger.getLogger(TestClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            processRequest(request, response);
        } catch (NamingException ex) {
            Logger.getLogger(TestClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
