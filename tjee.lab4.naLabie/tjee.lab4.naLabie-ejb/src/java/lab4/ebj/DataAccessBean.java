package lab4.ebj;

import java.util.Date;
import javax.ejb.Stateful;
//import javax.ejb.Stateless;

@Stateful
public class DataAccessBean implements DataAccessBeanRemote {
    private String OstatnieDane;
    private Date OstatniDostep;
    private int licznik;

    @Override
    public String getOstatnieDane() {
        return OstatniDostep + "\n" + OstatnieDane;
    }

    @Override
    public void setOstatnieDane(String dane, Date data) {
        OstatnieDane = dane;
        OstatniDostep = data;
    }

    @Override
    public void increaseLicznik() {
        licznik++;
    }

    @Override
    public int getLicznik() {
        return licznik;
    }

    
    
}

